import nox


@nox.session(python="3.11")
def package(session):
    """Generate the distributable package."""

    session.install("build")
    session.run("python", "-m", "build")


@nox.session(python="3.11")
def black(session):
    """Check code format with black."""

    session.install("black")
    session.run("black", "--check", "--verbose", "--diff", ".")


@nox.session(python="3.11")
def flake8(session):
    """Check code with flake8."""

    session.install(".[dev]")
    session.run("flake8", "--verbose", ".")


@nox.session(python=["3.7", "3.8", "3.9", "3.10", "3.11"])
def tests(session):
    """Run the test suite."""

    session.install("--find-links=./dist", "convert3mf[dev]")
    session.run(
        "pytest",
        "--cov=convert3mf",
        "--cov-report",
        "term",
        "--cov-report",
        f"xml:./build/reports/coverage/{session.name}/coverage.xml",
        "--cov-report",
        f"html:./build/reports/coverage/{session.name}/html/",
        f"--junitxml=./build/test-results/{session.name}/results.xml",
    )
