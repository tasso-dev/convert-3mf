# Copyright (c) 2022 Andrew A. Tasso
#
# This work is licensed under the terms of the MIT license.
# The full terms of the license can be found in LICENSE.txt
# in the project root or at https://opensource.org/licenses/MIT

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="convert3mf",
    version="0.1.0a1",
    description="A simple utility for converting 3D Manufacturing Format (3MF) "
    "formatted files",
    url="https://gitlab.com/tasso-dev/convert3mf",
    author="Andrew A. Tasso",
    license="MIT",
    license_files=("LICENSE.txt"),
    long_description=long_description,
    long_description_content_type="text/markdown",
    project_urls={
        "Issue Tracker": "https://gitlab.com/tasso-dev/convert3mf/-/issues",
        "Source": "https://gitlab.com/tasso-dev/convert3mf.git",
    },
    keywords="3mf stl 3d printing converter",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "Intended Audience :: Manufacturing",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.7",
        "Topic :: Multimedia :: Graphics :: 3D Modeling",
        "Topic :: Multimedia :: Graphics :: 3D Rendering",
        "Topic :: Multimedia :: Graphics :: Graphics Conversion",
    ],
    entry_points={"console_scripts": ["convert3mf = convert3mf.__main__:main"]},
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.7",
    install_requires=[
        "click==8.1.3",
        # Backport to support Click version lookup in a Python 3.7 environment.
        # Remove after Python 3.8 becomes minimum version.
        "importlib-metadata==4.2.0; python_version < '3.8'",
    ],
    extras_require={
        "dev": [
            "black==22.6.0",
            "nox==2022.8.7",
            "pre-commit==2.20.0",
            "pytest==7.1.2",
            "pytest-cov==3.0.0",
            "flake8==5.0.3",
        ],
    },
)
