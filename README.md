# convert3mf

A simple utility for [converting 3D Manufacturing Format](https://3mf.io/specification/) (3MF) formatted files


[[_TOC_]]

# Usage

```shell
convert3mf [OPTIONS] INPUT 
```


## Arguments

| **Arguments**  | **Description**                                            | **Type** | **Valid Values**         |
| -------------- | ---------------------------------------------------------- | -------- | ------------------------ |
| `INPUT`        | The path of the file to be converted (including filename). | Path     | Any 3mf file that exists |


## Options

| **Option**          | **Description**                                            | **Type** | **Valid Values**       | **Default Value**     |
| --------------      | ---------------------------------------------------------- | -------- | ---------------------- | --------------------- |
| `-o, --output FILE` | The path to write the converted file (including filename). | Path     | Any non-directory path | `converted.stl`       |                   |
| `-H, --help`        | Show the help message and exit.                            | N/A      | N/A                    | N/A                   |
| `-V, --version`     | Show the version and exit.                                 | N/A      | N/A                    | N/A                   |


# Runtime Requirements

`terra` requires, at minimum, python version `3.7` and has no additional system dependencies. 

Any python package requirements are defined by [`setup.py`](setup.py) and will be automatically installed when 
[installing](#installation) the [wheel distribution](#building) of this package.


# Building

`convert3mf` is built using the [`build`](https://pypi.org/project/build/) package as configured by 
[`setup.py`](setup.py) located in the root of this project.


**Usage (from the root of the project):**
```shell
python -m build
```

This will build the package in an isolated environment, generating a source-distribution and wheel package in the 
`dist/` directory located in the root of this project.


# Installation


## Sources

`convert3mf` can be installed from multiple sources using `pip`.

---


### PyPI 

The official source of `convert3mf` is the (Python Package Index (PyPI))[https://pypi.org/].

To install `convert3mf` from the official repo, execute the following:

```shell
pip install convert3mf
```

---


### Built Wheel File

Alternatively, if need be, `convert3mf` can be installed using the [`wheel`](https://pypi.org/project/wheel/) package 
obtained by other means or [manually built](#building)


To install the wheel file, execute the following:

```shell
pip install convert3mf-<version>-py3-none-any.whl
```


# License

## MIT License

Copyright (c) 2022 Andrew A. Tasso

This work is licensed under the terms of the MIT license. The full terms of the license can be found in 
[LICENSE.txt](LICENSE.txt) in the project root or at https://opensource.org/licenses/MIT


## Third Party

This project utilizes 3rd party libraries that are distributed under their own terms.
