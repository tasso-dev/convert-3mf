# Copyright (c) 2022 Andrew A. Tasso
#
# This work is licensed under the terms of the MIT license.
# The full terms of the license can be found in LICENSE.txt
# in the project root or at https://opensource.org/licenses/MIT

import pytest

from click.testing import CliRunner
from convert3mf.__main__ import main
from typing import Optional, Union, Sequence


try:
    from importlib import metadata  # type: ignore
except ImportError:
    # Backport to support Python earlier than 3.8
    import importlib_metadata as metadata  # type: ignore


@pytest.fixture()
def invoke_cli():

    runner = CliRunner()

    def cli_main(
        *cli_args: Optional[Union[str, Sequence[str]]],
        input_file_path: Optional[str] = None,
        input_file_contents: Optional[str] = None,
    ):

        with runner.isolated_filesystem():

            if input_file_path is not None and input_file_contents is not None:
                with open(input_file_path, "w") as f:
                    f.write(input_file_contents)

            return runner.invoke(main, *cli_args, prog_name="convert3mf")

    return cli_main


def test_cli__only_valid_input_file_specified___successful_execution(invoke_cli):

    result = invoke_cli(
        "test.3mf", input_file_path="test.3mf", input_file_contents="test!"
    )

    assert result.exit_code == 0


@pytest.mark.parametrize("output_option", ["--output", "-o"])
def test_cli__valid_input_file_and_output_specified__successful_execution(
    invoke_cli, output_option
):

    result = invoke_cli(
        ["test.3mf", output_option, "output.stl"],
        input_file_path="test.3mf",
        input_file_contents="test!",
    )

    assert result.exit_code == 0


@pytest.mark.parametrize("version_option", ["--version", "-V"])
def test_cli__version_option__version_information_displayed(invoke_cli, version_option):

    expected_version = metadata.version("convert3mf")

    result = invoke_cli(version_option)

    assert result.exit_code == 0
    assert f"convert3mf, version {expected_version}" in result.output


@pytest.mark.parametrize("help_option", ["--help", "-H"])
def test_cli__help_option__help_information_displayed(invoke_cli, help_option):

    result = invoke_cli(help_option)

    assert result.exit_code == 0
    assert "Usage: convert3mf [OPTIONS] INPUT" in result.output


def test_cli__missing_input_argument__usage_and_error_displayed(invoke_cli):

    result = invoke_cli()

    assert result.exit_code == 2
    assert "Usage: convert3mf [OPTIONS] INPUT" in result.output
    assert "Error: Missing argument 'INPUT'." in result.output


def test_cli__input_file_doesnt_exist__usage_and_error_displayed(invoke_cli):

    missing_file_path = "missing.3mf"

    result = invoke_cli(missing_file_path)

    assert result.exit_code == 2
    assert "Usage: convert3mf [OPTIONS] INPUT" in result.output
    assert (
        f"Error: Invalid value for 'INPUT': Path '{missing_file_path}' does not exist."
        in result.output
    )


@pytest.mark.parametrize("output_option", ["--output", "-o"])
def test_cli__output_option_missing_argument__error_displayed(
    invoke_cli, output_option
):

    runner = CliRunner()

    result = runner.invoke(main, ["test.3mf", output_option], prog_name="convert3mf")

    assert result.exit_code == 2
    assert f"Error: Option '{output_option}' requires an argument." in result.output


@pytest.mark.parametrize("invalid_option", ["--invalid", "-Q"])
def test_cli__invalid_option__usage_and_error_displayed(invoke_cli, invalid_option):

    result = invoke_cli(invalid_option)

    assert result.exit_code == 2
    assert "Usage: convert3mf [OPTIONS] INPUT" in result.output
    assert f"Error: No such option: {invalid_option}" in result.output
