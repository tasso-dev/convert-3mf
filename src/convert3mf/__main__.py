# Copyright (c) 2022 Andrew A. Tasso
#
# This work is licensed under the terms of the MIT license.
# The full terms of the license can be found in LICENSE.txt
# in the project root or at https://opensource.org/licenses/MIT

"""Allow convert3mf to be executed using `python -m convert3mf`."""
from convert3mf.cli import main


if __name__ == "__main__":  # pragma: no cover
    main(prog_name="convert3mf")
