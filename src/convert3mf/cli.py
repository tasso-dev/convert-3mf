# Copyright (c) 2022 Andrew A. Tasso
#
# This work is licensed under the terms of the MIT license.
# The full terms of the license can be found in LICENSE.txt
# in the project root or at https://opensource.org/licenses/MIT

import click


@click.command()
@click.argument(
    "input",
    type=click.Path(exists=True),
)
@click.option(
    "-o",
    "--output",
    type=click.Path(dir_okay=False),
    help="The path to write the converted file (including filename).",
    default="converted.stl",
)
@click.help_option(
    "-H",
    "--help",
)
@click.version_option(
    # None is specified as the version to cause Click to automatically look up
    # the version using package metadata.
    None,
    "-V",
    "--version",
)
def main(input, output):
    """Convert file specified by INPUT."""

    print(f"Hello World! Input: {input}, Output: {output}")
